archer_tools
------------

Repository for miscellaneous tools for running jobs on ARCHER. These were
designed for personal use, so don't be surprised if they don't work for you.
Feature requests and bugs may be considered.

* ``multi_sub``

  * Submit a single job to the queue that will run multiple tasks concurrently
    and spawn new tasks as others finish.
  * See ``--help`` for all available options.
  * Example: ``bin/multi_sub -w 24:00:00 -n 48 -t 4 -b e999-project`` will
    submit a job under budget code ``e999-project`` in the current directory
    that will send any ``.param`` files in subdirectories to the default version
    of CASTEP in the system. There will be four concurrent jobs running on
    12 nodes (288 cores) each that will be replaced as they finish.
  * Use the ``-d`` option to see the job that will be submitted.


Bugs & Feature Requests
=======================

The scripts are developed ad-hoc. Use the issue tracker on bitbucket
https://bitbucket.org/tdaff/archer_tools/issues to make report bugs and to
make requests for new features.

Installation
============

The scripts can be run directly from the bin directory, so no installation is
needed.

If you'd like to install the scripts, they use ``setup.py`` as a Python
package installation. Run one of the following to put the modules in your
``PYTHONPATH`` and individual scripts into your ``PATH``.

.. code-block::

    python setup.py install --user

Site wide installation (not recommended):

.. code-block::

    sudo python setup.py install
