"""
alog.py

Makeshift logging functions to separate logging output to stderr.

"""

from __future__ import print_function
import sys


def debug(*objs):
    """Write debugging output to stderr."""
    print("DEBUG: ", *objs, file=sys.stderr)


def info(*objs):
    """Write INFO level output to stderr."""
    print("INFO: ", *objs, file=sys.stderr)


def warning(*objs):
    """Write WARNING level output to stderr."""
    print("WARNING: ", *objs, file=sys.stderr)


def error(*objs):
    """Write ERROR level output to stderr."""
    print("ERROR: ", *objs, file=sys.stderr)
