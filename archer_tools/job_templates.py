"""
job_templates.py

Job scripts to run on the cluster. Contain boilerplate code, but need
to be filled in with the tasks required.

"""

import re
from os import path


def pbs_python_multi(resources, options, shell='python'):
    """
    Construct the appropriate pbs header.

    Construct a python script that submits jobs in subdirectories to use
    up resources and chain jobs after they finish.
    """
    header = []
    if shell.lower() in ('bash', 'sh'):
        header.append('#!/bin/bash\n\n')
    elif shell.lower() == 'python':
        header.append('#!/usr/bin/env python\n\n')

    job_name = options.job_name
    if job_name is not None:
        header.append('# The job name\n#PBS -N {0}\n'.format(job_name))
    else:
        header.append('# Unnamed job\n#PBS -N multi_job\n')

    # Assume resources object has dealt with the limits already

    if resources.queue is None:
        header.append("#PBS -l walltime={0}\n".format(resources.walltime))
        header.append("#PBS -l select={0}\n".format(resources.nodes))
    elif resources.queue == 'serial':
        header.append("#PBS -l select=serial=true:ncpus=1\n")
        header.append("#PBS -l walltime={0}\n".format(resources.walltime))
    else:
        header.append("#PBS -q {0}\n".format(resources.queue))
        header.append("#PBS -l walltime={0}\n".format(resources.walltime))
        header.append("#PBS -l select={0}\n".format(resources.nodes))

    header.append("#PBS -A {0}\n".format(resources.budget))

    if options.email:
        header.append("#PBS -m abe\n")
        header.append("#PBS -M {0}\n".format(options.email))

    with open(path.join(path.dirname(path.realpath(__file__)),
                        'python_multi.template')) as template_file:
        template = template_file.read()

    exe_name = options.exe
    modules = options.modules

    # Use the basename of the input file, rather than the filename
    basename = False
    # Does the command need to have output redirected to a file?
    shell_output = False

    if 'castep' in exe_name:
        include_file = '.param'
        exclude_file = '.castep'
        basename = True
    elif 'pw.x' in exe_name:
        include_file = '.pwi'
        exclude_file = '.pwo'
        if not exe_name.split()[-1] in ['-i', '-in', '-inp', '-input']:
            exe_name += ' -in'
        shell_output = True
    elif exe_name.split('path.pathsep')[-1].startswith('lmp'):
        include_file = '.in'
        exclude_file = '.out'
        shell_output = True
    else:
        include_file = '.param'
        exclude_file = '.castep'

    return "".join(header) + template.format(resources=resources,
                                             modules=repr(modules),
                                             include_file=include_file,
                                             exclude_file=exclude_file,
                                             exe_name=exe_name,
                                             basename=basename,
                                             shell_output=shell_output)


def pbs_bash_one(resources, options):
    """
    Create a script to run a single job. Includes some extra tweaks for
    different codes.
    """

    script = ['#!/bin/bash\n\n']

    job_name = options.job_name
    if job_name is not None:
        script.append('# The job name\n#PBS -N {0}\n'.format(job_name))
    elif options.input:
        join_name = "{0}-{1}".format(options.type, "-".join(options.input))
        script.append("# Constructed job name\n#PBS -N {:<0.15}\n"
                      "".format(join_name))
    else:
        script.append('# Unnamed job\n#PBS -N {0}_job\n'.format(options.type))

    # Join output and error
    script.append('#PBS -j oe\n')

    # Assume resources object has dealt with the limits already

    if resources.queue is None:
        script.append("#PBS -l walltime={0}\n".format(resources.walltime))
        script.append("#PBS -l select={0}\n".format(resources.nodes))
    elif resources.queue == 'serial':
        script.append("#PBS -l select=serial=true:ncpus=1\n")
        script.append("#PBS -l walltime={0}\n".format(resources.walltime))
    else:
        script.append("#PBS -q {0}\n".format(resources.queue))
        script.append("#PBS -l walltime={0}\n".format(resources.walltime))
        script.append("#PBS -l select={0}\n".format(resources.nodes))

    script.append("#PBS -A {0}\n".format(resources.budget))

    if options.email:
        script.append("#PBS -m abe\n")
        script.append("#PBS -M {0}\n".format(options.email))

    script.append("\n#Begin script contents\n")

    # Load any modules
    if not options.modules:
        if options.type == 'pw':
            options.modules = ['espresso']
        elif options.type == 'pp':
            options.modules = ['espresso']
        elif options.type == 'lmp':
            options.modules = ['lammps']

    for module in options.modules:
        script.append("module load {0}\n".format(module))

    script.append(
        "\nexport OMP_NUM_THREADS=1\n"
        "# resolve symbolic links to absolute path\n"
        "export PBS_O_WORKDIR=$(readlink -f $PBS_O_WORKDIR)\n"
        "# Change to the directory the job was submitted from\n"
        "cd $PBS_O_WORKDIR\n"
        "# Make sure temporary files go to the /work filesystem\n"
        "export TMPDIR=$PBS_O_WORKDIR\n")

    # Use default exe names if none are given, bind it to
    # the options object
    if not options.exe:
        if options.type == 'pw':
            options.exe = 'pw.x'
        elif options.type == 'pp':
            options.exe = 'pp.x'
        elif options.type == 'lmp':
            options.exe = 'lmp_xc30'
        else:
            raise KeyError('No exe provided for unknown job type '
                           '{}'.format(options.type))

    if options.args:
        args = " ".join(options.args)
    else:
        args = ""

    if options.type == 'pw':
        script.append("export ESPRESSO_TMPDIR=$PBS_O_WORKDIR\n\n")
        for input_file in options.input:
            output_file = re.sub(r'\.in$|\.pwi$|$', '.pwo', input_file)
            script.append(
                "# Run the Quantum Espresso executable 'pw.x'\n"
                "aprun -n {resources.cpus_per_task} {exe} {args} "
                "-in {input_file} >> {output_file}\n".format(
                    resources=resources, exe=options.exe, args=args,
                    input_file=input_file, output_file=output_file))
    elif options.type == 'pp':
        script.append("export ESPRESSO_TMPDIR=$PBS_O_WORKDIR\n\n")
        for input_file in options.input:
            output_file = re.sub(r'\.in$|\.ppi$|$', '.ppo', input_file)
            script.append(
                "# Run the Quantum Espresso executable 'pp.x'\n"
                "aprun -n {resources.cpus_per_task} {exe} {args} "
                "-in {input_file} >> {output_file}\n".format(
                    resources=resources, exe=options.exe, args=args,
                    input_file=input_file, output_file=output_file))
    elif options.type == 'lmp':
        for input_file in options.input:
            output_file = re.sub(r'\.in$|$', '.out', input_file)
            script.append(
                "# Run the lammps executable 'lmp_xc30'\n"
                "aprun -n {resources.cpus_per_task} {exe} {args} "
                "-in {input_file} >> {output_file}\n".format(
                    resources=resources, exe=options.exe, args=args,
                    input_file=input_file, output_file=output_file))

    return "".join(script)
