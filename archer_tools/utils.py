"""
utils.py

Collection of utility classes and functions that have no obvious home.
"""

from archer_tools.alog import warning


class Walltime(object):
    """
    Interact with time strings as if they were times and inter-convert between
    representations.
    """

    def __init__(self, walltime):
        """
        Time object that can inter-convert between representations.
        """
        if isinstance(walltime, Walltime):
            self._seconds = walltime.seconds
        else:
            try:
                self._seconds = self.string_to_seconds(walltime)
            except AttributeError:
                self._seconds = int(walltime)

    def is_longer_than(self, test_time):
        """
        Return True if this walltime is longer than the time given in the string
        representation.

        :param test_time: either a walltime objectm a string of 'hh:mm:ss' or
                          a number of seconds.
        :return: True if the walltime is longer than the test_time period
        """
        test_time = Walltime(test_time)
        return self.seconds > test_time.seconds

    # Make comparisons work with either strings or walltimes
    def __lt__(self, other):
        return self.seconds < Walltime(other).seconds

    def __le__(self, other):
        return self.seconds <= Walltime(other).seconds

    def __eq__(self, other):
        return self.seconds == Walltime(other).seconds

    def __ne__(self, other):
        return self.seconds != Walltime(other).seconds

    def __gt__(self, other):
        return self.seconds > Walltime(other).seconds

    def __ge__(self, other):
        return self.seconds >= Walltime(other).seconds

    def __repr__(self):
        return "{0}.{1}('{2}')".format(self.__module__,
                                       self.__class__.__name__,
                                       self.string)

    def __str__(self):
        return self.string

    @property
    def string(self):
        """Time as hh:mm:ss format."""
        return self.seconds_to_string(self._seconds)

    @string.setter
    def string(self, value):
        """Time as hh:mm:ss format."""
        self._seconds = self.string_to_seconds(value)

    @property
    def seconds(self):
        """Time in seconds."""
        return self._seconds

    @seconds.setter
    def seconds(self, value):
        """Time in seconds."""
        self._seconds = int(value)

    @staticmethod
    def string_to_seconds(walltime):
        """Convert a hh:mm:ss string to the number of seconds."""
        seconds = sum(int(x)*y for x, y in
                      zip(reversed(walltime.split(':')), [1, 60, 3600]))
        return seconds

    @staticmethod
    def seconds_to_string(seconds):
        """Return the number of seconds as a hh:mm:ss representation."""
        hours, seconds = divmod(seconds, 3600)
        minutes, seconds = divmod(seconds, 60)
        walltime = "{0:02d}:{1:02d}:{2:02d}".format(hours, minutes, seconds)
        return walltime


QUEUES = {
    None: {
        'max_time': Walltime('24:00:00'),
        'max_nodes': 4920},
    'long': {
        'max_time': Walltime('48:00:00'),
        'max_nodes': 256},
    'short': {
        'max_time': Walltime('00:20:00'),
        'max_nodes': 8},
    'low': {
        'max_time': Walltime('03:00:00'),
        'max_nodes': 512},
    'serial': {
        'max_time': Walltime('24:00:00'),
        'max_nodes': 1},
}


class Resources(object):
    """
    Resources requested by the job. Ensures jobs fit within limits during
    initialisation.

    Limits are based on:
    http://www.archer.ac.uk/documentation/user-guide/batch.php#sec-5.8
    """
    def __init__(self, queue=None, nodes=1, walltime=0, tasks=None,
                 cpus_per_task=None, budget=None, cpus_per_node=24):
        """
        Set all requested resources within the limits defined bu that queue.

        Parameters
        ----------
        queue : str or None
            Name of queue requested in the system. Defaults to regular.
        nodes : int
            Number of nodes requested (24 CPUs in each node).
        walltime : str or int or Walltime
            Maximum runtime for the job, usually as 'hh:mm:ss'.
        tasks : int, optional
            Number of jobs to run concurrently. Defaults to 1.
        cpus_per_task : int, optional
            Number of cpus to dedicate to each concurrent task. This value is
            ignored if tasks is also specified.
        cpus_per_node : int, optional
            CPUs to be used on each node. Defaults to full nodes on ARCHER.
        """

        # Make sure these are in a format we can deal with
        walltime = Walltime(walltime)
        nodes = int(nodes)

        # Regular queue is no queue
        if queue not in QUEUES:
            warning("Unknown queue {0} defaulting to regular.")
            queue = None
        self.queue = queue

        # Trim resources to fit within the limits specified by the queue
        # but don't adjust the queue ever.
        if walltime > QUEUES[queue]['max_time']:
            warning("Adjusting {0} job time from {1} to maximum {2}.".format(
                queue, walltime.string, QUEUES[queue]['max_time'].string))
            self.walltime = QUEUES[queue]['max_time']
        else:
            self.walltime = walltime

        if nodes > QUEUES[queue]['max_nodes']:
            warning("Adjusting {0} nodes from {1} to maximum {2}.".format(
                queue, nodes, QUEUES[queue]['max_nodes']))
            self.nodes = QUEUES[queue]['max_nodes']
        else:
            self.nodes = nodes

        self.cpus_per_node = cpus_per_node
        if tasks is not None:
            self.tasks = tasks
        elif cpus_per_task is not None:
            self.cpus_per_task = cpus_per_task
        else:
            self.tasks = 1

        if budget is not None:
            self.budget = budget

    @property
    def cpus_per_task(self):
        """Divide up the number of nodes into the number of tasks."""
        return (self.cpus_per_node*self.nodes)/self.tasks

    @cpus_per_task.setter
    def cpus_per_task(self, value):
        """Assign the number of tasks based ont the cpus per task."""
        self.tasks = (self.cpus_per_node*self.nodes)/value
